package PageObjects;

import Core.Wrapper.Element;
import Models.Product;
import Utils.TestResult;
import io.qameta.allure.Step;

public class ItemDetailPage extends BasePage {
    private Element headerName = new Element("//div[@class = 'header']//h1[@class = 'title']");
    private Element price = new Element("//div[@class = 'product-price__current-price']");

    @Step("Checking Product Info is correct")
    public TestResult isProductInfoCorrect(Product expectedProduct) {
        TestResult result = new TestResult();
        this.headerName.waitForDisplay();
        result.checkEqual(this.headerName.getText(), expectedProduct.getName(), "ProductName");
        result.checkEqual(this.headerName.getText(), expectedProduct.getName(), "ProductName");
        return result;
    }
}
