package PageObjects;

import Core.Wrapper.Driver;
import Core.Wrapper.Element;
import Utils.TestResult;
import io.qameta.allure.Step;

public class MainPage extends BasePage {
    private Element title = new Element("//h2[@class='c-fxpromo-title']");
    String now;

    @Step("Checking MainPage display correctly")
    public TestResult isMainPageDisplayCorrect(String searchFieldPLaceHolder, String buttonText) {
        TestResult result = new TestResult();
        this.mainSearchField.waitForDisplay();
        result.checkEqual(this.mainSearchField.getAttribute("placeholder"), searchFieldPLaceHolder, "Search field");
        result.checkEqual(this.mainSearchButton.getText(), buttonText, "Search button");
        return result;
    }

    @Step("Select Menu Item")
    public void selectMenuItem(String breadcrumbString) {
        String[] breadcrumbsItems = breadcrumbString.split(" > ");
        this.menuButton.waitForDisplay();
        this.menuButton.moveTo();
        this.dynamicNavigateItem.setDynamicLocator(breadcrumbsItems[0]).moveTo();
        this.dynamicMenuItem.setDynamicLocator(breadcrumbsItems[1]).click();
    }

    public void gotoMainPage() throws InterruptedException {
        Driver.closeAndSwitchToOtherWindow();
    }

    public String gotoNewPage() throws InterruptedException {
        Driver.createAndSwitchToNewTab("https://www.mozilla.org/vi");
        return this.title.getText();

    }
}
