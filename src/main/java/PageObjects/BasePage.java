package PageObjects;

import Core.Wrapper.Element;

public class BasePage {
    protected Element mainSearchField = new Element("//input[@data-view-id='main_search_form_input']");
    protected Element mainSearchButton = new Element("//button[@data-view-id='main_search_form_button']");
    protected Element menuButton = new Element("//a[@class='Menu-button']");
    protected Element dynamicNavigateItem = new Element("//a[@data-view-id='main_navigation_item']//span[text() = '%s']");
    protected Element accountContainer = new Element("//div[@data-view-id='header_header_account_container']/span");
    protected Element breadcrumbItems = new Element("//a[@class = 'breadcrumb-item']//span");
    protected Element dynamicMenuItem = new Element("//span[text() = '%s']/ancestor::li//a[text() = '%s']/ancestor::li[contains(@class, 'SubMenu')]//a[text() = '%s']");
    //div[contains(@class ,'styles__Root')]

    public void searchItem(String item) {
        this.mainSearchField.enter(item);
        this.mainSearchButton.click();
    }

}
