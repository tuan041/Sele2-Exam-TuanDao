package PageObjects;

import Core.Wrapper.Element;
import Enums.SearchPageSideBarItem;
import Models.Product;
import Utils.TestResult;
import io.qameta.allure.Step;

import java.util.List;

public class SearchPage extends BasePage {
    private Element dynamicProductName = new Element("(//a[@class = 'product-item']//div[@class = 'name']//h3)[%s]");
    private Element dynamicProductPrice = new Element("(//a[@class = 'product-item']//div[@class = 'price-discount__price'])[%s]");
    private Element dynamicProductItem = new Element("(//a[@class = 'product-item']//div[@class = 'name'])[%s]");
    private Element titleHeader = new Element("//div[@class = 'title']/h1");
    private Element dynamicSideSelection = new Element("//div[h4[text() = '%s']]//div[span[text() = '%s']]/preceding-sibling::span");
    private Element serviceSelection = new Element("//div[h4[text() = 'DỊCH VỤ']]//div[span[text() = 'Giao siêu tốc 2H']]/preceding-sibling::span");
    private Element productTopList = new Element("//div[@data-view-id='product_list_top_categories_container']");
    private Element dynamicSideInput = new Element("//input[@placeholder = '%s']");
    private Element sideInputButton = new Element("//button[@data-view-id ='search_filter_submit_button']");
    public Product selectedProduct;

    @Step("Checking SearchPage display correctly")
    public TestResult isSearchPageDisplayCorrect(String breadcrumb, String header) {
        TestResult result = new TestResult();
        this.titleHeader.waitForDisplay();
        List<String> breadcrumbListItems = this.breadcrumbItems.getAllTexts();
        String breadcrumbString = this.buildBreadcrumbItemsString(breadcrumbListItems);
        result.checkEqual(breadcrumbString, breadcrumb, "Breadcrumb");
        result.checkEqual(this.titleHeader.getText(), header, "Title Header");
        return result;
    }

    private String buildBreadcrumbItemsString(List<String> items) {
        return String.join(" > ", items);
    }

    private Product getProductInfo(int itemIndex) {
        return new Product(this.dynamicProductName.setDynamicLocator(itemIndex).getText(), this.dynamicProductPrice.setDynamicLocator(itemIndex).getText());
    }

    @Step("Select Product")
    public void selectProduct(int itemIndex) {
        this.selectedProduct = this.getProductInfo(itemIndex);
        this.dynamicProductItem.setDynamicLocator(itemIndex).click();
    }

    @Step("Select Tiki Trading vendor")
    public void selectVendor(String vendor) {
//        ElementWrapper vendorElement = this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.VENDOR.getValue(), vendor);
        this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.VENDOR.getValue(), vendor).scrollToAndClick();
//        DriverWrapper.getInstance().SleepInSecond(2);
//        this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.VENDOR.getValue(), vendor).check();
    }

    @Step("Select Tiki Now service")
    public void selectService(String service) {
//        ElementWrapper serviceElement = this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.SERVICE.getValue(), service);
//        DriverWrapper.getInstance().SleepInSecond(2);
        this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.SERVICE.getValue(), service).scrollToAndClick();
//        this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.SERVICE.getValue(), service).check();
    }

    @Step("Input Price Range")
    public void inputPriceRange(int from, int to) {
//        ElementWrapper serviceElement = this.dynamicSideSelection.setDynamicLocator(SearchPageSideBarItem.SERVICE.getValue(), service);
        this.dynamicSideInput.setDynamicLocator("Giá từ").waitForDisplay();
        this.dynamicSideInput.scrollTo();
        this.dynamicSideInput.enter(String.valueOf(from));
        this.dynamicSideInput.waitForDisplay();
        this.dynamicSideInput.enter(String.valueOf(to));
        this.sideInputButton.click();

    }
}
