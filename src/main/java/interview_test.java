import java.util.Arrays;

public class interview_test {
    public static void main(String[] args) {
        System.out.println("");
        int[] b = new int[]{-6, -91, 1011, -100, 84, -22, 0, 1, 473};
        System.out.println(maxNumberMultiplesOfThree(b));
    }

    private static int maxNumberMultiplesOfThree(int[] A) {
        return Arrays.stream(A)
                .filter(ele -> (ele % 3) == 0)
                .max().getAsInt();
    }


}
