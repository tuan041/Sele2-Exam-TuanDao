package Core.Wrapper;

import Models.BoundingClientRect;
import Utils.Constant;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class ElementWrapperOld {
    private final static Logger log = LogManager.getLogger(ElementWrapperOld.class);
    /*
    To make the code more stable, I chose to wait longer (Constant.WAIT_TIMEOUT = 20s)
    instead of shorter (WAIT_MEDIUM_TIMEOUT = 10 or WAIT_SHORT_TIMEOUT = 5) in Wait methods,
    In Wait methods, I don't throw errors, I just use log to log them.
     */
    private final int eleTimeout = Constant.WAIT_TIMEOUT;// in second

    /*
     Use this for checking methods.
     */
    private final int actionTimeout = Constant.WAIT_MEDIUM_TIMEOUT;// in second
    private By by;
    private String xpath;
    private final WebDriver webDriver = Driver.getDriver();
    private final Actions action = new Actions(webDriver);
    private Object[] parameter;

    public ElementWrapperOld() {
    }

    public ElementWrapperOld(By by) {
        this.by = by;
    }

    public ElementWrapperOld(String locator) {
        this.xpath = locator;
        this.by = By.xpath(this.xpath);
    }

    /*
     This is the Xpath-oriented ver. I chose this design for the framework, so there is no need to use this constructor.
     We can use String to manage xpath in Page Object instead of declaring a new ElementWrapper to make it an argument.
     */

//    public ElementWrapper(ElementWrapper parent, String locator) {
//        this.xpath = parent.getElementXpath() + locator;
//        this.by = By.xpath(this.xpath);
//    }

    public ElementWrapperOld setDynamicLocator(Object... parameter) {
        this.by = By.xpath(String.format(this.xpath, parameter));
        this.parameter = parameter;
        return this;
    }

    public WebElement getElement() {
        return webDriver.findElement(this.by);
    }

    public String getElementXpath() {
        return String.format(this.xpath, this.parameter);
    }

    public List<WebElement> getElementList() {
        try {
            return webDriver.findElements(this.by);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getLocatorFromBy() {
        String byString = this.by.toString();
        String locator = null;
        Matcher m = Pattern.compile(".+:\\s(.+)").matcher(byString);
        while (m.find()) {
            locator = m.group(1);
        }
        return locator;
    }

    public List<String> getAllTexts() {
        try {
            return this.getElementList().stream().map(x -> x.getText().trim())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getAllInnerTexts() {
        try {
            return this.getAttributeList("innerText");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getAllValues() {
        try {
            return this.getAttributeList("value");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getAttributeList(String attribute) {
        try {
            return this.getElementList().stream().map(x -> x.getAttribute(attribute).trim())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void waitForDisplay() {
        this.waitForDisplay(this.eleTimeout);
    }

    public void waitForDisplay(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.visibilityOfElementLocated(this.by));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitForPresence() {
        this.waitForPresence(this.eleTimeout);
    }

    public void waitForPresence(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.presenceOfElementLocated(this.by));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitForClickable() {
        this.waitForClickable(this.eleTimeout);
    }

    public void waitForClickable(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.elementToBeClickable(this.by));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitUntilDisappear() {
        this.waitUntilDisappear(this.eleTimeout);
    }

    public void waitUntilDisappear(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.invisibilityOfElementLocated(this.by));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitUntilNotPresence(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(this.by)));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitForStaleness() {
        this.waitForStaleness(this.eleTimeout);
    }

    public void waitForStaleness(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.stalenessOf(this.getElement()));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitForSelected() {
        this.waitForSelected(this.eleTimeout);
    }

    public void waitForSelected(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.elementToBeSelected(this.getElement()));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitUntilEnabled() {
        this.waitUntilEnabled(this.eleTimeout);
    }

    public void waitUntilEnabled(long timeOut) {
        try {
            this.waitForClickable(timeOut);
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public boolean isClickable() {
        return this.isClickable(this.actionTimeout);
    }

    public boolean isClickable(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.elementToBeClickable(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isEnabled() {
        return this.isEnabled(this.actionTimeout);
    }

    public boolean isEnabled(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.elementToBeClickable(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public boolean isEnabled(long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.startClock();
//        try {
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0) {
//                if (this.getElement().isEnabled()) {
//                    return true;
//                }
//                Thread.sleep(500);
//            }
//            return false;
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

//    public boolean isExist() {
//        try {
//            if (this.getElementList().size() > 0) {
//                return true;
//            }
//            return false;
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public boolean isPresent() {
        return this.isPresent(this.actionTimeout);
    }


    public boolean isPresent(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.presenceOfElementLocated(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public boolean isPresent(long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.startClock();
//        try {
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0) {
//                if (this.getElementList().size() > 0) {
//                    return true;
//                }
//                Thread.sleep(500);
//            }
//            return false;
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }


    public boolean isNotPresent() {
        return this.isNotPresent(this.actionTimeout);
    }

    public boolean isNotPresent(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(this.by)));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isDisplayed() {
        return this.isDisplayed(this.actionTimeout);
    }

    public boolean isDisplayed(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.visibilityOfElementLocated(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public boolean isDisplayed(long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.startClock();
//        boolean state = false;
//        try {
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0) {
//                try {
//                    state = this.getElement().isDisplayed();
//                } catch (NoSuchElementException | StaleElementReferenceException e) {
//                    state = false;
//                }
//                Thread.sleep(500);
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//        return state;
//    }

    public boolean isNotDisplay() {
        return this.isNotDisplay(this.actionTimeout);
    }

    public boolean isNotDisplay(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.invisibilityOfElementLocated(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isSelected() {
        return this.isSelected(this.actionTimeout);
    }

//    public boolean isSelected(long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.startClock();
//        try {
//            this.waitForClickable();
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0) {
//                if (this.getElement().isSelected()) {
//                    return true;
//                }
//            }
//            return false;
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public boolean isSelected(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until(ExpectedConditions.elementToBeSelected(this.by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void rightClick() {
        try {
            this.waitForClickable();
            this.action.contextClick(this.getElement()).perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void doubleClick() {
        try {
            this.waitForClickable();
            this.action.doubleClick(this.getElement()).perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void check() {
        try {
            if (!this.isSelected())
                this.click();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void uncheck() {
        try {
            if (this.isSelected())
                this.click();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void click() {
        this.click(5);
    }

    public void click(int maxTries) {
        log.debug(String.format("Click %s: %s", this.getElementXpath(), maxTries));
        int count = 1;
        while (count <= maxTries) {
            try {
                this.waitForClickable();
                this.action.click(this.getElement()).perform();
                break;
            } catch (Exception e) {
                ++count;
                if (count > maxTries) {
                    log.error(e.getMessage());
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void type(String value) {
        try {
            this.waitForDisplay();
            this.getElement().sendKeys(value);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void moveTo() {
        try {
            this.waitForDisplay();
            this.action.moveToElement(this.getElement()).perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void moveTo(Point point) {
        this.moveTo(point, this.eleTimeout);
    }

    public void moveTo(Point point, long timeOut) {
        try {
            this.waitForDisplay(timeOut);
            this.action.moveToElement(this.getElement(), point.getX(), point.getY()).perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public void moveToAndClick() {
        try {
            this.moveTo();
            this.click();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void pressButton(Keys button) {
        try {
            this.waitForClickable();
            this.moveTo();
            this.getElement().sendKeys(button);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }


    public void clear() {
        try {
            this.waitForClickable();
            this.getElement().clear();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void enter(String value) {
        try {
            this.clear();
            this.type(value);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getAttribute(String attributeName) {
        return this.getAttribute(attributeName, this.eleTimeout);
    }

    public String getAttribute(String attributeName, long timeOut) {
        try {
            this.waitForPresence(timeOut);
            return this.getElement().getAttribute(attributeName);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getCssValue(String cssName) {
        return this.getCssValue(cssName, this.eleTimeout);
    }

    public String getCssValue(String cssName, long timeOut) {
        try {
            this.waitForPresence(timeOut);
            return this.getElement().getCssValue(cssName);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getText() {
        return this.getText(this.eleTimeout);
    }

    public String getText(long timeOut) {
        try {
            this.waitForDisplay(timeOut);
            return this.getElement().getText().trim();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getValue() {
        return this.getValue(this.eleTimeout);
    }

    public String getValue(long timeOut) {
        try {
            this.waitForPresence(timeOut);
            return this.getAttribute("value", timeOut).trim();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getInnerText() {
        return this.getInnerText(this.eleTimeout);
    }

    public String getInnerText(long timeOut) {
        try {
            return this.getAttribute("innerText", timeOut).trim();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void waitUntilPropertyChange(String property) {
        this.waitUntilPropertyChange(property, this.eleTimeout);
    }

    public void waitUntilPropertyChange(String property, long timeOut) {
        try {
            final String[] previousProperty = {this.getAttribute(property)};
            final String[] currentProperty = {previousProperty[0]};

            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> {
                        if (!previousProperty[0].equals(currentProperty[0])) {
                            return true;
                        }
                        currentProperty[0] = this.getAttribute(property);
                        return false;
                    });
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilPropertyChange(String property, long timeOut) {
//        try {
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.startClock();
//
//            String previousValue = this.getAttribute(property);
//            String currentValue = previousValue;
//
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (previousValue.equals(currentValue))) {
//                currentValue = this.getAttribute(property);
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public void waitUntilPropertyNotChange(String property) {
        this.waitUntilPropertyNotChange(property, this.eleTimeout);
    }

    public void waitUntilPropertyNotChange(String property, long timeOut) {
        try {
            final String[] previousProperty = {"previousProperty"};
            final String[] currentProperty = {this.getAttribute(property)};

            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> {
                        if (previousProperty[0].equals(currentProperty[0])) {
                            return true;
                        }
                        previousProperty[0] = currentProperty[0];
                        currentProperty[0] = this.getAttribute(property);
                        return false;
                    });
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilPropertyNotChange(String property, long timeOut) {
//        try {
//            String previousValue = "previousValue";
//            String currentValue = this.getAttribute(property);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.startClock();
//
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (!previousValue.equals(currentValue))) {
//                previousValue = currentValue;
//                currentValue = this.getAttribute(property);
//                Thread.sleep(500);
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public void waitUntilCssValueNotChange(String property) {
        this.waitUntilCssValueNotChange(property, this.eleTimeout);
    }

    public void waitUntilCssValueNotChange(String cssName, long timeOut) {
        try {
            final String[] previousValue = {"previousValue"};
            final String[] currentValue = {this.getCssValue(cssName)};

            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> {
                        if (previousValue[0].equals(currentValue[0])) {
                            return true;
                        }
                        previousValue[0] = currentValue[0];
                        currentValue[0] = this.getCssValue(cssName);
                        return false;
                    });
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilCssValueNotChange(String cssName, long timeOut) {
//        try {
//            String previousValue = "previousValue";
//            String currentValue = this.getCssValue(cssName);
//
//            StopWatch stopWatch = new StopWatch();
//            stopWatch.startClock();
//
//            while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (!previousValue.equals(currentValue))) {
//                previousValue = currentValue;
//                currentValue = this.getCssValue(cssName);
//                Thread.sleep(500);
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public void waitForControlStable() {
        this.waitForControlStable(this.eleTimeout);
    }

    public void waitForControlStable(long timeOut) {
        try {
            this.waitUntilCssValueNotChange("top", timeOut);
            this.waitUntilCssValueNotChange("height", timeOut);
            this.waitUntilCssValueNotChange("left", timeOut);
            this.waitUntilCssValueNotChange("width", timeOut);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void scrollTo() {
        try {
            Driver.jsExecution("arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'nearest'});", this.getElement());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

//    public void scrollToElement() {
//        try {
//            DriverWrapper.jsExecution(String.format("let element= document.evaluate(\"%s\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue ; element.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'nearest'})", this.getElementXpath()));
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

//    public void jsClick() {
//        try {
//            DriverWrapper.jsExecution(String.format("let element= document.evaluate(\"%s\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue ; element.click({behavior: 'smooth', block: 'center', inline: 'nearest'})", this.getElementXpath()));
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public void jsClick() {
        try {
            Driver.jsExecution("arguments[0].click({behavior: 'smooth', block: 'center', inline: 'nearest'})", this.getElement());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void scrollToAndClick() {
        int retry = 3;
        while (true) {
            try {
                this.waitForPresence();
                this.scrollTo();
                this.click();
                break;
            } catch (StaleElementReferenceException e) {
                --retry;
                if (retry <= 0) {
                    log.error(e.getMessage());
                    throw new RuntimeException(e);
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new RuntimeException(e);
            }
        }
    }

    public List<WebElement> getChildrenElementList() {
        try {
            return this.getElement().findElements(By.xpath("./*"));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getChildrenInnerText() {
        try {
            List<WebElement> children = this.getChildrenElementList();
            return children.stream().map(element -> element.getAttribute("innerText").trim())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getChildrenText() {
        try {
            List<WebElement> children = this.getChildrenElementList();
            return children.stream().map(element -> element.getText().trim())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Dimension getSize() {
        try {
            this.waitForDisplay();
            return this.getElement().getSize();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Point getLocation() {
        try {
            this.waitForDisplay();
            return this.getElement().getLocation();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getTagName() {
        try {
            this.waitForPresence();
            return this.getElement().getTagName();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void dragAndDrop(Point dragPoint, Point dropPoint) {
        this.dragAndDrop(dragPoint, dropPoint, this.eleTimeout);
    }

    public void dragAndDrop(Point dragPoint, Point dropPoint, long timeOut) {
        try {
            this.waitForClickable(timeOut);
            this.moveTo(dragPoint);
            this.action.clickAndHold().perform();
            this.moveTo(dropPoint);
            this.action.release().perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void dragAndDropBy(Point dropPoint) {
        this.dragAndDropBy(dropPoint, this.eleTimeout);
    }

    public void dragAndDropBy(Point dropPoint, long timeOut) {
        try {
            this.waitForClickable(timeOut);
            this.action.dragAndDropBy(this.getElement(), dropPoint.getX(), dropPoint.getY()).perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void selectByPartialTextOption(String text) {
        try {
            this.waitForPresence();
            List<WebElement> options = new Select(this.getElement()).getOptions().stream()
                    .filter(o -> o.getText().contains(text))
                    .collect(Collectors.toList());
            options.get(0).click();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void selectByTextOption(String text) {
        try {
            this.waitForPresence();
            new Select(this.getElement()).selectByVisibleText(text);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void selectByValueOption(String value) {
        try {
            this.waitForPresence();
            new Select(this.getElement()).selectByValue(value);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void selectByIndexOption(int index) {
        try {
            this.waitForPresence();
            new Select(this.getElement()).selectByIndex(index);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getTextFirstSelectedOption() {
        try {
            this.waitForPresence();
            WebElement option = new Select(this.getElement()).getFirstSelectedOption();
            return option.getText().trim();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public List<String> getTextAllSelectedOption() {
        try {
            this.waitForPresence();
            return new Select(this.getElement()).getAllSelectedOptions().stream().map(x -> x.getText().trim())
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

//    public void selectOption(String text) {
//        this.selectOption(text, false);
//    }
//
//    public void selectOption(String text, boolean bool) {
//        try {
//            Select select = new Select(this.getElement());
//            if (bool) {
//                select.selectByVisibleText(text);
//            } else {
//                select.selectByValue(text);
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }
//
//    public void selectOption(int index) {
//        try {
//            Select select = new Select(this.getElement());
//            select.selectByIndex(index);
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            throw new RuntimeException(e);
//        }
//    }

    public BoundingClientRect getBoundingRect() {
        try {
            this.waitForPresence();
            JavascriptExecutor jsExecutor = (JavascriptExecutor) webDriver;
            Gson gson = new Gson();
            return gson.fromJson((jsExecutor.executeScript("return JSON.stringify(arguments[0].getBoundingClientRect());", this.getElement()).toString()), BoundingClientRect.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void waitUntilCssValueChangeToExpected(String cssName, String expectedValue) {
        this.waitUntilCssValueChangeToExpected(cssName, expectedValue, this.eleTimeout);
    }

    public void waitUntilCssValueChangeToExpected(String cssName, String expectedValue, long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> this.getElement().getCssValue(cssName).equals(expectedValue));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilCssValueChangeToExpected(String cssName, String expectedValue, long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        String currentValue = " ";
//        stopWatch.startClock();
//        while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (!currentValue.equals(expectedValue))) {
//            try {
//                currentValue = this.getCssValue(cssName);
//                Thread.sleep(1000);
//            } catch (Exception e) {
//                log.error(e.getMessage());
//                throw new RuntimeException(e);
//            }
//        }
//    }


    public void waitUntilTextChangeTo(String text) {
        this.waitUntilTextChangeTo(text, this.eleTimeout);
    }

    public void waitUntilTextChangeTo(String text, long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> this.getElement().getText().equals(text));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

    public void waitUntilTextNotEmpty() {
        this.waitUntilTextNotEmpty(this.eleTimeout);
    }

    public void waitUntilTextNotEmpty(long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> !this.getElement().getText().isEmpty());
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilTextChange(String text, long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        String currentText = "";
//        stopWatch.startClock();
//        while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (!currentText.equals(text))) {
//            try {
//                currentText = this.getText();
//                Thread.sleep(500);
//            } catch (Exception e) {
//                log.error(e.getMessage());
//                throw new RuntimeException(e);
//            }
//        }
//    }

    public void waitUntilValueChangeTo(String value) {
        this.waitUntilValueChangeTo(value, this.eleTimeout);
    }

    public void waitUntilValueChangeTo(String value, long timeOut) {
        try {
            new WebDriverWait(this.webDriver, Duration.ofSeconds(timeOut))
                    .until((condition) -> this.getElement().getAttribute("value").equals(value));
        } catch (Exception e) {
            log.error(e.getMessage());
//            throw new RuntimeException(e);
        }
    }

//    public void waitUntilValueChange(String value, long timeOut) {
//        StopWatch stopWatch = new StopWatch();
//        String currentValue = "";
//        stopWatch.startClock();
//        while (stopWatch.getTimeLeftInSecond(timeOut) > 0 && (!currentValue.equals(value))) {
//            try {
//                currentValue = this.getValue();
//                Thread.sleep(500);
//            } catch (Exception e) {
//                log.error(e.getMessage());
//                throw new RuntimeException(e);
//            }
//        }
//    }

    public void takeElementScreenshot(String fileWithPath) {
        try {
            this.waitForDisplay();
            File source = this.getElement().getScreenshotAs(OutputType.FILE);
            File destination = new File(fileWithPath);
            FileHandler.copy(source, destination);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public byte[] takeElementScreenshot() {
        try {
            this.waitForDisplay();
            return this.getElement().getScreenshotAs(OutputType.BYTES);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String takeElementScreenshot64Bytes() {
        try {
            this.waitForDisplay();
            return this.getElement().getScreenshotAs(OutputType.BASE64);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}



