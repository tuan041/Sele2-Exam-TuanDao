public class ParityDegree {

    public static void main(String[] args) {
        System.out.println(largestPowerOf2WhichDividesN(32));
    }

//    public static int largestPowerOf2WhichDividesN(final int N) {
//        int result = 0;
//        for (int i = 0; i < N / 2; i++) {
//            int num = (int) Math.pow(2, i);
//            if (N % num == 0) {
//                result = i;
//            }
//        }
//        return result;
//    }
//
//    private static int caculatePower(final int index) {
//        double d = Math.pow(2, index);
//        return (int) d;
//    }

    public static int largestPowerOf2WhichDividesN(final int N) {
        if (N < 2) {
            return 0;
        }
        int n = N;
        while ((n & (n - 1)) != 0) {
            n = n & (n - 1);
        }
        return n;
    }
}