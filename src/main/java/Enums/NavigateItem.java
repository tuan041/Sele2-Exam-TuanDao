package Enums;

public enum NavigateItem {
    CELL_PHONE("cellphone"),
    TV("tv"),
    HEAD_PHONE("headphone"),
    LAPTOP("laptop"),
    CAMERA("camera"),
    ELECTRONIC("electronic"),
    PAN("pan"),
    SPRAYER("sprayer"),
    BOTTLE("bottle"),
    LIPSTICKS("lipsticks"),
    TSHIRT("tshirt"),
    SPORT("sport"),
    BIKE("bike"),
    GLOBAL("global"),
    BOOK("book"),
    VOUCHER("voucher");

    private final String item;

    NavigateItem(String value) {
        this.item = value;
    }

    public String getValue() {
        return item;
    }
}
