package Enums;

public enum Elements {
    LOGOUT("Logout"),
    ADMINISTER("Administer"),
    DATA_PROFILES("Data Profiles"),
    PANELS("Panels"),
    OVERVIEW("Overview"),
    EXECUTION_DASHBOARD("Execution Dashboard"),
    ADD_PAGE("Add Page"),
    CREATE_PROFILE("Create Profile"),
    CREATE_PANEL("Create Panel"),
    EDIT("Edit"),
    DELETE("Delete"),
    CHECK("check"),
    UNCHECK("uncheck"),
    RADIO_CHART("Chart"),
    DRP_NAME("Name"),
    DIALOG_EDIT_PAGE("Edit Page");

    private final String element;

    Elements(String value) {
        this.element = value;
    }

    public String getValue() {
        return element;
    }
}
