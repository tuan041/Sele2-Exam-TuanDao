package Enums;

public enum Panels {
    SERIES_NAME("Name"),
    SERIES_LOCATION("Location"),
    TYPE_CHART("Chart"),
    TYPE_INDICATOR("Indicator"),
    TYPE_REPORT("Report"),
    TYPE_HEATMAP("Heat Map"),
    STYLE_2D("2D"),
    STYLE_3D("3D"),
    LEGEND_NONE("None"),
    LEGEND_TOP("Top"),
    LEGEND_RIGHT("Right"),
    LEGEND_BOTTOM("Bottom"),
    LEGEND_LEFT("Left");


    private final String panel;

    Panels(String value) {
        this.panel = value;
    }

    public String getValue() {
        return panel;
    }
}
