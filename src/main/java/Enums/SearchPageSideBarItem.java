package Enums;

public enum SearchPageSideBarItem {
    VENDOR("Nhà cung cấp"),
    SERVICE("DỊCH VỤ"),
    PRICE("Giá");

    private final String item;

    SearchPageSideBarItem(String value) {
        this.item = value;
    }

    public String getValue() {
        return item;
    }
}
