package Enums;

public enum Repositories {
    SAMPLE_REPOSITORY("SampleRepository"),
    TEST_REPOSITORY("TestRepository");

    private final String repository;

    Repositories(String value) {
        this.repository = value;
    }

    public String getValue() {
        return repository;
    }
}
