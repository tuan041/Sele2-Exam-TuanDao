package Enums;

public enum Alert {
    OK("ok"),
    CANCEL("cancel"),
    CLOSE("close"),
    INVALID_LOGIN("Username or password is invalid"),
    DUPLICATE_PANEL("Duplicated panel already exists. Please enter a different name."),
    INVALID_DISPLAY_NAME("Invalid display name. The name cannot contain high ASCII characters or any of the following characters: /:*?<>|\"#[]{}=%;");
    private final String page;

    Alert(String value) {
        this.page = value;
    }

    public String getValue() {
        return page;
    }
}
