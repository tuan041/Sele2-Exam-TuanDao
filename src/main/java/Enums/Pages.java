package Enums;

public enum Pages {
    EXECUTION_DASHBOARD("Execution Dashboard");

    private final String page;

    Pages(String value) {
        this.page = value;
    }

    public String getValue() {
        return page;
    }
}
