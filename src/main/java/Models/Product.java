package Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
    private String name;
    private String price;

    public Product(String _name, String _price) {
        this.name = _name;
        this.price = _price;
    }
}
