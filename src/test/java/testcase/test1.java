package testcase;

import Core.Wrapper.Driver;
import Core.Wrapper.Element;
import Models.BoundingClientRect;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;


public class test1 extends TestBase1 {
    private static Logger logger = LogManager.getLogger(test1.class);

    @Step("method1")
    public void sprintSt() {
        System.out.println("aaaaaaaaaaaaaaaaa");
    }

    @Test(description = "First test")
    public void test() {
        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury";
        String actualTitle = "";

        this.sprintSt();
        // launch Fire fox and direct it to the Base URL
        Driver.get(baseUrl);
        Element specialTable = new Element("//table[tbody[tr[td[img[@src='images/hdr_specials.gif']]]]]");
        logger.info("fafa");
        logger.error("xxx");
        BoundingClientRect rect = specialTable.getBoundingRect();
        System.out.println(rect.top);
        System.out.println(rect.right);
        System.out.println(rect.bottom);
        System.out.println(rect.left);
        System.out.println(rect.width);
        System.out.println(rect.height);
        System.out.println(rect.x);
        System.out.println(rect.y);

        // get the actual value of the title
        actualTitle = Driver.getTitle();

        /*
         * compare the actual title of the page with the expected one and print
         * the result as "Passed" or "Failed"
         */
        Assert.assertEquals(actualTitle, expectedTitle);
    }

    @Test(description = "Second test")
    public void test1() {
        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";
        String a = " ";
        String b = " ";
        Assert.assertEquals(a, b, "asdasdas");
        this.sprintSt();
        // launch Fire fox and direct it to the Base URL
        Driver.get(baseUrl);
        Element specialTable = new Element("//table[tbody[tr[td[img[@src='images/hdr_specials.gif']]]]]");
        logger.info("fafa");
        BoundingClientRect rect = specialTable.getBoundingRect();
        System.out.println(rect.top);
        System.out.println(rect.right);
        System.out.println(rect.bottom);
        System.out.println(rect.left);
        System.out.println(rect.width);
        System.out.println(rect.height);
        System.out.println(rect.x);
        System.out.println(rect.y);

        // get the actual value of the title
        actualTitle = Driver.getTitle();

        /*
         * compare the actual title of the page with the expected one and print
         * the result as "Passed" or "Failed"
         */
        Assert.assertEquals(actualTitle, expectedTitle);
    }
}
