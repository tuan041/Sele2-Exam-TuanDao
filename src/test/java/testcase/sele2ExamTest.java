package testcase;

import Models.Product;
import PageObjects.ItemDetailPage;
import PageObjects.MainPage;
import PageObjects.SearchPage;
import Utils.TestResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

public class sele2ExamTest extends TestBase {
    private static Logger logger = LogManager.getLogger(sele2ExamTest.class);

    @Test(description = "Verify the product information loaded correctly")
    public void TC1() throws InterruptedException {
        MainPage mainPage = new MainPage();
        SearchPage searchPage = new SearchPage();
        ItemDetailPage itemDetailPage = new ItemDetailPage();

//        devTools.getDomains().javascript().addJsBinding("document.evaluate(\"//h2[@class='c-fxpromo-title']/text()\", document, null, XPathResult.STRING_TYPE).stringValue");

        String title = mainPage.gotoNewPage();
        Assert.assertEquals(title, "Lấy lại những ý tưởng hay – nhanh hơ", "Fail 1");

//        mainPage.gotoMainPage();
//
//        String searchFieldText = "Tìm sản phẩm, danh mục hay thương hiệu mong muốn ...";
//        String searchButtonText = "Tìm Kiếm";
//        String searchItemName = "Điện thoại";
//        String searchPageBreadcrumb = "Trang chủ > Điện thoại";
//        String searchPageHeader = "Kết quả tìm kiếm  cho `điện thoại`";
//
//        logger.info("VP. Verify that MainPage display correctly");
//        TestResult result = mainPage.isMainPageDisplayCorrect(searchFieldText, searchButtonText);
//        Assert.assertTrue(result.ok(), result.getError());
//
//        logger.info("Search Item");
//        mainPage.searchItem(searchItemName);
//
//        logger.info("VP. Verify that SearchPage display correctly");
//        searchPage.isSearchPageDisplayCorrect(searchPageBreadcrumb, searchPageHeader);
//
//        logger.info("Select Product");
//        searchPage.selectProduct(2);
//        Product selectedProduct = searchPage.selectedProduct;
//
//        logger.info("VP. Verify that Selected Item Info in Item Detail Page display correctly");
//        itemDetailPage.isProductInfoCorrect(selectedProduct);
    }

    @Test(description = "Verify the product information loaded correctly")
    public void TC2() throws InterruptedException {
        MainPage mainPage = new MainPage();
        SearchPage searchPage = new SearchPage();
        ItemDetailPage itemDetailPage = new ItemDetailPage();

        String title = mainPage.gotoNewPage();
        Assert.assertEquals(title, "Lấy lại những ý tưởng hay – nhanh hơn", "Fail 2");

        mainPage.gotoMainPage();

        String searchFieldText = "Tìm sản phẩm, danh mục hay thương hiệu mong muốn ...";
        String searchButtonText = "Tìm Kiếm";
        String searchItemName = "Điện thoại";
        String searchPageBreadcrumb = "Trang chủ > Điện thoại";
        String searchPageHeader = "Kết quả tìm kiếm   cho `điện thoại`";

        logger.info("VP. Verify that MainPage display correctly");
        TestResult result = mainPage.isMainPageDisplayCorrect(searchFieldText, searchButtonText);
        Assert.assertTrue(result.ok(), result.getError());

        logger.info("Search Item");
        mainPage.searchItem(searchItemName);

        logger.info("VP. Verify that SearchPage display correctly");
        searchPage.isSearchPageDisplayCorrect(searchPageBreadcrumb, searchPageHeader);

        logger.info("Select Product");
        searchPage.selectProduct(2);
        Product selectedProduct = searchPage.selectedProduct;

        logger.info("VP. Verify that Selected Item Info in Item Detail Page display correctly");
        itemDetailPage.isProductInfoCorrect(selectedProduct);
    }

//    @Test(description = "Test case 2")
//    public void TC2() {
//        MainPage mainPage = new MainPage();
//        SearchPage searchPage = new SearchPage();
//        ItemDetailPage itemDetailPage = new ItemDetailPage();
//
//        String searchFieldText = "Tìm sản phẩm, danh mục hay thương hiệu mong muốn ...";
//        String searchButtonText = "Tìm Kiếm";
//        String searchItemName = "Điện thoại";
//        String searchPageBreadcrumb = "Trang chủ > Điện thoại";
//        String searchPageHeader = "Kết quả tìm kiếm cho `điện thoại`";
//
//        logger.info("VP. Verify that MainPage display correctly");
//        TestResult result = mainPage.isMainPageDisplayCorrect(searchFieldText, searchButtonText);
//        Assert.assertTrue(result.ok(), result.getError());
//
//        logger.info("Search Item");
//        mainPage.searchItem(searchItemName);
//
//        logger.info("VP. Verify that SearchPage display correctly");
//        searchPage.isSearchPageDisplayCorrect(searchPageBreadcrumb, searchPageHeader);
//
//        logger.info("Select Product");
//        searchPage.selectProduct(2);
//        Product selectedProduct = searchPage.selectedProduct;
//
//        logger.info("VP. Verify that Selected Item Info in Item Detail Page display correctly");
//        itemDetailPage.isProductInfoCorrect(selectedProduct);
//    }

//    @Test(description = "Verify user can filter search condition for product")
//    public void TC2() {
//        MainPage mainPage = new MainPage();
//        SearchPage searchPage = new SearchPage();
//
//        String breadcrumbString = "Điện Gia Dụng > Đồ dùng nhà bếp > Lò vi sóng";
//        String searchPageBreadcrumb = "Trang chủ > Điện Gia Dụng > Đồ dùng nhà bếp > Lò vi sóng";
//        String searchPageHeader = "Lò vi sóng";
//        String vendor = "Tiki Trading";
//        String service = "Giao siêu tốc 2H";
//        int priceFrom = 1000000;
//        int priceTo = 2000000;
//
//        logger.info("Select Menu Item");
//        mainPage.selectMenuItem(breadcrumbString);
//
//        logger.info("VP. Verify that SearchPage display correctly");
//        searchPage.isSearchPageDisplayCorrect(searchPageBreadcrumb, searchPageHeader);
//
//        logger.info("Select Tiki Trading Vendor");
//        searchPage.selectVendor(vendor);
//
//        logger.info("Select Tiki Now Service");
//        searchPage.selectService(service);
//
//        logger.info("Input Price Range");
//        searchPage.inputPriceRange(priceFrom, priceTo);
//    }

}
