package testcase;

import Core.Wrapper.Driver;
import Core.Wrapper.Element;
import Models.BoundingClientRect;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.InputEvent;


public class test2 extends TestBase1 {
    private static Logger logger = LogManager.getLogger(test2.class);

    @Test(description = "First test")
    public void test() throws InterruptedException {
//        String baseUrl = "https://sortablejs.github.io/Vue.Draggable/";
//        Driver.get(baseUrl);
//
//        Element itemJohn = new Element("//div[@class='list-group']//div[normalize-space(.)='John']");
//        Element itemJoao = new Element("//div[@class='list-group']//div[normalize-space(.)='Joao']");
//        Element itemJean = new Element("//div[@class='list-group']//div[normalize-space(.)='Jean']");
//
//        itemJohn.dragAndDrop(itemJohn.getLocation(), itemJean.getLocation());
//        Thread.sleep(2000);
//        itemJohn.dragAndDrop(itemJohn.getLocation(), itemJean.getLocation());
//        Thread.sleep(2000);

        Driver.get("https://bestvpn.org/html5demos/drag/");
        new Element(By.cssSelector("#five")).moveTo();

        Element from1 = new Element(By.cssSelector("#one"));
        Element to1 = new Element(By.cssSelector("#bin"));
        Element from2 = new Element(By.cssSelector("#two"));

        Thread.sleep(2000);
        from1.dragAndDropJS(to1);
        Thread.sleep(2000);
        from2.dragAndDropJS(to1);
        Thread.sleep(2000);
    }

    @Test
    public void handleDragAndDrop() throws InterruptedException {
        Actions action = new Actions(Driver.getDriver());

        Driver.get("http://demo.guru99.com/test/drag_drop.html");
        Element fromElement1 = new Element("//a[normalize-space()='BANK']");
        Element toElement1 = new Element("(//div[@id='shoppingCart1']//div)[1]");

        Element fromElement2 = new Element("(//li[@id='fourth'])[2]");
        Element toElement2 = new Element("(//div[@id='shoppingCart4']//div)[1]");

        fromElement1.dragAndDrop(toElement1);

        Thread.sleep(2000);
        fromElement2.dragAndDropElement(toElement2);
        Thread.sleep(2000);
    }

    @Test
    public void handleDragAndDropOffset() throws Exception {
        Actions action = new Actions(Driver.getDriver());
        Driver.get("https://david-desmaisons.github.io/draggable-example/");
        Thread.sleep(1000);

        Element fromElement1 = new Element("(//li[@class='list-group-item'])[1]");
        Element toElement1 = new Element("(//li[@class='list-group-item'])[2]");
//        Driver.getDriver().manage().window().maximize();
        BoundingClientRect fromBox = fromElement1.getBoundingRect();
        BoundingClientRect toBox = toElement1.getBoundingRect();
        System.out.println(fromBox.toString());
        System.out.println(toBox.toString());

//
//        int X1 = driver.findElement(fromElement1).getLocation().getX();
//        int Y1 = driver.findElement(fromElement1).getLocation().getY();
        System.out.println(fromElement1.getLocation().getX() + " , " + fromElement1.getLocation().getY());
//
//        int X2 = driver.findElement(toElement1).getLocation().getX();
//        int Y2 = driver.findElement(toElement1).getLocation().getY();
        System.out.println(toElement1.getLocation().getX() + " , " + toElement1.getLocation().getY());

//        fromElement1.dragAndDropRobot(toElement1, 0);
//
//        //Chổ này lấy theo toạ độ cụ thể. Chả biết sao nó lấy toạ độ Element chênh lệch vậy nữa =))
        Thread.sleep(1000);
        Robot robot = new Robot();
        //monitor 4k , display 3840 x 2160
        robot.mouseMove((int) ((fromBox.x + fromBox.width / 2) * 1.5), (int) ((fromBox.y + fromBox.height / 2) * 2.125));
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
//
        Thread.sleep(1000);
        robot.mouseMove((int) ((toBox.x + toBox.width / 2) * 1.5), (int) ((toBox.y + toBox.height / 2) * 2.125));

        Thread.sleep(1000);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        Thread.sleep(1000);

//
//        Thread.sleep(3000);
    }

    @Test
    public void htmlMessage() throws InterruptedException {
        Element loginButton = new Element("//input[@class='btn btn-signin']");
        Element emailField = new Element("//input[@id='customer_email'][@required]");
        Element passwordField = new Element("//input[@id='customer_password][@required]");

        Driver.get("https://www.guardian.com.vn/account/login");
        emailField.waitForPresence();
        String message = (String) Driver.jsExecution("return arguments[0].validationMessage;", emailField.getElement());
        System.out.println(message);

//        System.out.println(emailField.getAttribute("validationMessage"));
    }
}
