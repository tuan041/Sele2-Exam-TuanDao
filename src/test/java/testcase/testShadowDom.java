package testcase;

import Core.Wrapper.Driver;
import Core.Wrapper.Element;
import Core.Wrapper.ShadowElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;


public class testShadowDom extends TestBase1 {
    private static Logger logger = LogManager.getLogger(testShadowDom.class);


    @Test(description = "First test")
    public void test() throws InterruptedException {
        Driver.get("https://selectorshub.com/xpath-practice-page/");
        Element loader = new Element("//div[@id='loader']");
        ShadowElement learningHub = new ShadowElement("div[id='userName']", "a[class='learningHub']");
        ShadowElement testWait = new ShadowElement("div[id='userName']", "a[class='learningH']");
        ShadowElement iframe = new ShadowElement("div[id='userName']", "iframe[id='pact1']");
        ShadowElement testerFood = new ShadowElement("#snacktime", "a[class='food']");
        Element userEmailField = new Element("//div[@class='userform']//input[@id='userId']");
        Element iframePact = new Element(By.id("pact"));
        Driver.waitForPageLoad();
//        loader.waitForDisplay();
//        loader.waitUntilDisappear();

        System.out.println(learningHub.getText());
        learningHub.scrollTo();
        iframe.scrollTo();

        iframe.waitForControlStable();
        Driver.switchToFrame(iframe.getShadowElement());
//        Driver.switchToFrame("pact");
        Element textFileInIframe = new Element("//input[@id = 'jex']");
        textFileInIframe.enter("hahahaha");
        Driver.switchToDefault();
        testWait.waitForDisplay(3);

        // iframePact.scrollTo();
        // testWait.waitForDisplay(3);

        Driver.switchToFrame(iframePact.getElement());
        testerFood.scrollToAndClick();
        // testWait.waitForDisplay(3);

        // issue just can use jsClick or type(Keys.ENTER) to Click
        // testerFood.type(Keys.ENTER);
        // use this wait can wait at least 2s, still don't know why waitForClickAble is not effective,
        // or it's effective but element click is not effective
        testerFood.waitForControlStable();
        testerFood.click();
        Driver.switchToDefault();

        Driver.switchToOtherWindow();
        Driver.waitForPageLoad();
        Driver.closeAndSwitchToOtherWindow();
        learningHub.scrollTo();
        iframe.scrollTo();
        testWait.waitForDisplay(3);
    }

}
