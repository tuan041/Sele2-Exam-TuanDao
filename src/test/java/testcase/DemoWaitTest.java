package testcase;

import Core.Wrapper.Driver;
import Core.Wrapper.Element;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DemoWaitTest extends TestBase1 {
    SoftAssert softAssert = new SoftAssert();


    @Test
    public void positiveTest() {
        String url = "http://demo.guru99.com/test/newtours/";
        String radioButtonAndCheckBoxUrl = "https://demo.guru99.com/test/radio.html";
        Element btnSubmit = new Element("//input[@type='submit']");
        Element radioButton1 = new Element("//input[@id='vfb-7-1']");
        Element checkBox1 = new Element("//input[@id='vfb-6-0']");
        Element wrongButton = new Element("//input[@id='wrong-button-xpath']");

        Driver.get(url);
        softAssert.assertTrue(btnSubmit.isPresent(), "Submit button should be present");
        softAssert.assertTrue(btnSubmit.isDisplayed(), "Submit button should be Displayed");
        softAssert.assertTrue(btnSubmit.isEnabled(), "Submit button should be Enabled");

//        softAssert.assertTrue(radioButton1.isNotPresent(), "Radio button 1 should not be present");
//        softAssert.assertTrue(radioButton1.isNotDisplayed(), "Radio button 1 should not be Displayed");

//        radioButton1.waitForDisplay(5);
        softAssert.assertTrue(radioButton1.isNotEnabled(), "Radio button 1 should not be Enabled");
        softAssert.assertTrue(radioButton1.isNotSelected(), "Radio button 1 should not be selected");

        Driver.get(radioButtonAndCheckBoxUrl);
        softAssert.assertTrue(radioButton1.isNotSelected(), "Radio button 1 should not be selected");
        softAssert.assertTrue(checkBox1.isNotSelected(), "checkbox 1 should not be selected");

        radioButton1.scrollToAndClick();
        checkBox1.scrollToAndClick();

        softAssert.assertTrue(radioButton1.isSelected(), "Radio button 1 should be selected");
        softAssert.assertTrue(checkBox1.isSelected(), "checkbox 1 should be selected");

        softAssert.assertAll();
    }

    @Test
    public void negativeTest() {
        String url = "http://demo.guru99.com/test/newtours/";
        String radioButtonAndCheckBoxUrl = "https://demo.guru99.com/test/radio.html";
        Element btnSubmit = new Element("//input[@type='submit']");
        Element radioButton1 = new Element("//input[@id='vfb-7-1']");
        Element checkBox1 = new Element("//input[@id='vfb-6-0']");
        Element wrongButton = new Element("//input[@id='wrong-button-xpath']");

        Driver.get(url);
        softAssert.assertTrue(checkBox1.isPresent(), "checkbox 1 should be present");
        softAssert.assertTrue(radioButton1.isDisplayed(), "Radio button 1 should be Displayed");
        softAssert.assertTrue(checkBox1.isEnabled(), "checkbox 1 should be Enabled");

        Driver.get(radioButtonAndCheckBoxUrl);
        softAssert.assertTrue(radioButton1.isSelected(), "Radio button 1 should be selected");
        softAssert.assertTrue(checkBox1.isSelected(), "checkbox 1 should be selected");

        radioButton1.scrollToAndClick();
        checkBox1.scrollToAndClick();

        softAssert.assertTrue(radioButton1.isNotSelected(), "Radio button 1 should not be selected");
        softAssert.assertTrue(checkBox1.isNotSelected(), "checkbox 1 should not be selected");
    }

}
